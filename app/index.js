const express = require('express')
const app = express()
var itemAtual = 0;
var votosUtilizados = [];
const pauta = ["Sobras","Cotas"];

app.get('/', (req, res) => res.send('Sistema de votação'))

//envia voto
app.get('/enviaVoto',function(req, res){
  var req.query.tag;
    if(!votoJaUtilizado(voto)){
      salvarVoto(voto);
      res.send('Computando voto tag::'+voto);
    }else{
      res.send('Voto já utilizado::'+voto);
    }

});
//ExibeTela
app.get('/exibeTela', (req, res) => res.send(getURLTela()))

//AvançaItem
app.get('/avancaItemPauta',function(req, res){
    avancarItemPauta();
    res.send('Item atual:'+itemAtual);

});
//ExibeResumo
app.get('/exibeResumo', (req, res) => res.send('ok'))

function salvarVoto(tag){
  votosUtilizados.push(tag);
  console.log("salvando o voto :"+tag+" no item "+itemAtual+" da pauta");
}

function avancarItemPauta(){
  // garante que o ponteiro da pauta não ultrapasse a quantidade de itens em pauta
  if(pauta.length > (itemAtual + 1)){
    itemAtual++;
  }
}

function votoJaUtilizado(voto){
  return votosUtilizados.includes(voto);
}








//SERVER


const server = app.listen(3001, () => console.log('Example app listening on port 3000!'))

setInterval(() => server.getConnections(
    (err, connections) => console.log(`${connections} connections currently open`)
), 61000);

process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);

let connections = [];

server.on('connection', connection => {
    connections.push(connection);
    connection.on('close', () => connections = connections.filter(curr => curr !== connection));
});

function shutDown() {
    console.log('Encerrando sistema');
    server.close(() => {
        console.log('Fechando conexões');
        process.exit(0);
    });

    setTimeout(() => {
        console.error('Aguardando outros processos');
        process.exit(1);
    }, 10000);

    connections.forEach(curr => curr.end());
    setTimeout(() => connections.forEach(curr => curr.destroy()), 5000);
}
